Yet another Revolut stonks balance calculator.

Sometimes you just want to know overall yearly status of your stonks (eg. for filling taxes) .
Revolut is great for microtrading, but does'nt help much with overall stats. All they provide - monthly statements in pdf format. 
So, this is personal experiment with portfolio value calculations.

1. export statemens manually from app

2. combine to one pdf:  
    $pdftk 2020.*.pdf output full2020.pdf

3. OCR pdf - yes, really, I don't found another solution to remove "random" hidden text (seems Revolut pdf generator adds parts of too long strings as hidden object to almost random locations???). Very long procedure, well - once a year :)
    $ocrmypdf  --force-ocr full2020.pdf full2020-ocr.pdf

4. grep BUY|SELL from pdf  
    $pdfgrep -h 'BUY|SELL' full2020-ocr.pdf > ALL.txt

5. run this horrible bash/sed/awk skript :)  
    $./calc.sh ALL.txt > ALL.csv
    
6. use *office  
    $libreoffice --calc ALL.csv



#!/bin/bash

YEAR=2020
FN=$1

#pdftk 2020.*.pdf output full2020.pdf
#ocrmypdf  --force-ocr full2020.pdf full2020-ocr.pdf
#pdfgrep -h 'BUY|SELL' full2020-ocr.pdf > ALL
#./calc.sh ALL > ALL.csv

cat $FN | grep $YEAR | sed 's/^[ \t]*//;s/[ \t]*$//' > $FN.tmp

awk -F'[ -]+' -v OFS=, '{
  quantity=$(NF-2); $(NF-2)="";
  price=$(NF-1); $(NF-1)="";
  amount=$(NF); $(NF)="";
  currency=$3; $3="";
  type=$4; $4="";
  symbol=$5; $5="";
  text="";

  for (i=5; i<(NF-2); i++) {
    text=text" "$(i)
    $(i)=""
  }

  if (match(amount, "[()]")) { 
    gsub(/[()]/,"", amount)
    amount="-"amount
  }; 

  portfolio[symbol]+=amount;

  $2=quantity;
  $3=price;
  $4=amount;
  $5=currency;
  $6=type;
  $7=symbol;
  $8=text;
}1
END {
  for (i in portfolio) {
    print "", "", "", portfolio[i], "USD", "BALANCE", i
  }
}' $FN.tmp

#libreoffice --calc ALL.csv
